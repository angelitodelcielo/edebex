<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        // edebex_calculator_homepage
        if ($pathinfo === '/calculator') {
            return array (  '_controller' => 'Edebex\\CalculatorBundle\\Controller\\DefaultController::indexAction',  '_route' => 'edebex_calculator_homepage',);
        }

        // login
        if ($pathinfo === '/login') {
            return array (  '_controller' => 'Edebex\\AuthentificationBundle\\Controller\\LoginController::indexAction',  '_route' => 'login',);
        }

        if (0 === strpos($pathinfo, '/admin')) {
            if (0 === strpos($pathinfo, '/admin/log')) {
                // login_check
                if ($pathinfo === '/admin/login_check') {
                    return array('_route' => 'login_check');
                }

                // logout
                if ($pathinfo === '/admin/logout') {
                    return array('_route' => 'logout');
                }

            }

            if (0 === strpos($pathinfo, '/admin/invoice')) {
                // admin_list
                if (rtrim($pathinfo, '/') === '/admin/invoice') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'admin_list');
                    }

                    return array (  '_controller' => 'Edebex\\AdminBundle\\Controller\\AdminController::indexAction',  '_route' => 'admin_list',);
                }

                // admin_single
                if (preg_match('#^/admin/invoice/(?P<reference>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_single;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_single')), array (  '_controller' => 'Edebex\\AdminBundle\\Controller\\AdminController::showAction',));
                }
                not_admin_single:

                // admin_edit
                if (preg_match('#^/admin/invoice/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PUT') {
                        $allow[] = 'PUT';
                        goto not_admin_edit;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_edit')), array (  '_controller' => 'Edebex\\AdminBundle\\Controller\\AdminController::editAction',));
                }
                not_admin_edit:

            }

        }

        // invoice_index
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'invoice_index');
            }

            return array (  '_controller' => 'Edebex\\AppBundle\\Controller\\InvoiceController::indexAction',  '_route' => 'invoice_index',);
        }

        // invoice_create
        if ($pathinfo === '/') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_invoice_create;
            }

            return array (  '_controller' => 'Edebex\\AppBundle\\Controller\\InvoiceController::createAction',  '_route' => 'invoice_create',);
        }
        not_invoice_create:

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}

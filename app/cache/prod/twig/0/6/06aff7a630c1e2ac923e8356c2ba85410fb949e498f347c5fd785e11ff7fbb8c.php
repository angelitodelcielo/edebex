<?php

/* EdebexAdminBundle::layout.html.twig */
class __TwigTemplate_185df55d3bdb19c827c88801dda4df91781f7fabcfe0701a7710096f15d09c01 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a02abfefae0894d80fd070cec9449a25530299524dea6d5539615a568c7dbbb0 = $this->env->getExtension("native_profiler");
        $__internal_a02abfefae0894d80fd070cec9449a25530299524dea6d5539615a568c7dbbb0->enter($__internal_a02abfefae0894d80fd070cec9449a25530299524dea6d5539615a568c7dbbb0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EdebexAdminBundle::layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>

    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">

    <title>";
        // line 8
        $this->displayBlock('title', $context, $blocks);
        echo " </title>

    <link href=\"/css/bootstrap.min.css\" rel=\"stylesheet\">
    <link href=\"/css/font-awesome.min.css\" rel=\"stylesheet\">
    <link href=\"/css/plugins/iCheck/custom.css\" rel=\"stylesheet\">
    <link href=\"/css/animate.css\" rel=\"stylesheet\">
    <link href=\"/css/style.css\" rel=\"stylesheet\">

</head>
<body>

\t";
        // line 19
        $this->displayBlock('body', $context, $blocks);
        // line 22
        echo "
</body>
</html>";
        
        $__internal_a02abfefae0894d80fd070cec9449a25530299524dea6d5539615a568c7dbbb0->leave($__internal_a02abfefae0894d80fd070cec9449a25530299524dea6d5539615a568c7dbbb0_prof);

    }

    // line 8
    public function block_title($context, array $blocks = array())
    {
        $__internal_7ac6bb67eccb93fea4398f3327a324c0b11a1153aaf33e6ff98d3b2da7694a7d = $this->env->getExtension("native_profiler");
        $__internal_7ac6bb67eccb93fea4398f3327a324c0b11a1153aaf33e6ff98d3b2da7694a7d->enter($__internal_7ac6bb67eccb93fea4398f3327a324c0b11a1153aaf33e6ff98d3b2da7694a7d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Edebex";
        
        $__internal_7ac6bb67eccb93fea4398f3327a324c0b11a1153aaf33e6ff98d3b2da7694a7d->leave($__internal_7ac6bb67eccb93fea4398f3327a324c0b11a1153aaf33e6ff98d3b2da7694a7d_prof);

    }

    // line 19
    public function block_body($context, array $blocks = array())
    {
        $__internal_95bf465af42f598c9486d744a07f7ea89b398e5b170a7a5ddc18c12f0666aac2 = $this->env->getExtension("native_profiler");
        $__internal_95bf465af42f598c9486d744a07f7ea89b398e5b170a7a5ddc18c12f0666aac2->enter($__internal_95bf465af42f598c9486d744a07f7ea89b398e5b170a7a5ddc18c12f0666aac2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 20
        echo "
    ";
        
        $__internal_95bf465af42f598c9486d744a07f7ea89b398e5b170a7a5ddc18c12f0666aac2->leave($__internal_95bf465af42f598c9486d744a07f7ea89b398e5b170a7a5ddc18c12f0666aac2_prof);

    }

    public function getTemplateName()
    {
        return "EdebexAdminBundle::layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  76 => 20,  70 => 19,  58 => 8,  49 => 22,  47 => 19,  33 => 8,  24 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/* <head>*/
/* */
/*     <meta charset="utf-8">*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1.0">*/
/* */
/*     <title>{% block title %}Edebex{% endblock %} </title>*/
/* */
/*     <link href="/css/bootstrap.min.css" rel="stylesheet">*/
/*     <link href="/css/font-awesome.min.css" rel="stylesheet">*/
/*     <link href="/css/plugins/iCheck/custom.css" rel="stylesheet">*/
/*     <link href="/css/animate.css" rel="stylesheet">*/
/*     <link href="/css/style.css" rel="stylesheet">*/
/* */
/* </head>*/
/* <body>*/
/* */
/* 	{% block body %}*/
/* */
/*     {% endblock %}*/
/* */
/* </body>*/
/* </html>*/

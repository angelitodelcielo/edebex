<?php

/* EdebexAppBundle::layout.html.twig */
class __TwigTemplate_7fafaf6b05031c57bf00f8fd6fbb0b5edc8c18165731e9dc9e3effc6a6d3c223 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e69114f95200668e643c35ac7990cd18428a4e3c53cd4c099f67549252306bc3 = $this->env->getExtension("native_profiler");
        $__internal_e69114f95200668e643c35ac7990cd18428a4e3c53cd4c099f67549252306bc3->enter($__internal_e69114f95200668e643c35ac7990cd18428a4e3c53cd4c099f67549252306bc3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EdebexAppBundle::layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <meta name=\"description\" content=\"\">
    <meta name=\"author\" content=\"\">

    <title>";
        // line 10
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

    <!-- Bootstrap core CSS -->
    <link href=\"/css/bootstrap.min.css\" rel=\"stylesheet\">

    <!-- Animation CSS -->
    <link href=\"/css/animate.css\" rel=\"stylesheet\">
    <link href=\"/css/font-awesome.min.css\" rel=\"stylesheet\">

    <!-- Custom styles for this template -->
    <link href=\"/css/style.css\" rel=\"stylesheet\">
    
    <link href=\"/css/plugins/jQueryUI/jquery-ui-1.10.4.custom.min.css\" rel=\"stylesheet\">
</head>
<body id=\"page-top\" class=\"landing-page\">

\t";
        // line 26
        $this->displayBlock('body', $context, $blocks);
        // line 29
        echo "
</body>
</html>";
        
        $__internal_e69114f95200668e643c35ac7990cd18428a4e3c53cd4c099f67549252306bc3->leave($__internal_e69114f95200668e643c35ac7990cd18428a4e3c53cd4c099f67549252306bc3_prof);

    }

    // line 10
    public function block_title($context, array $blocks = array())
    {
        $__internal_d991f3220d3768126bd4808bae504c785c7879f4346c8bf0d0d82829f568c3f4 = $this->env->getExtension("native_profiler");
        $__internal_d991f3220d3768126bd4808bae504c785c7879f4346c8bf0d0d82829f568c3f4->enter($__internal_d991f3220d3768126bd4808bae504c785c7879f4346c8bf0d0d82829f568c3f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Edebex";
        
        $__internal_d991f3220d3768126bd4808bae504c785c7879f4346c8bf0d0d82829f568c3f4->leave($__internal_d991f3220d3768126bd4808bae504c785c7879f4346c8bf0d0d82829f568c3f4_prof);

    }

    // line 26
    public function block_body($context, array $blocks = array())
    {
        $__internal_14e92c86302d5d412c22973712b0610cbc34d3115b9840372efb2a18b86bbc78 = $this->env->getExtension("native_profiler");
        $__internal_14e92c86302d5d412c22973712b0610cbc34d3115b9840372efb2a18b86bbc78->enter($__internal_14e92c86302d5d412c22973712b0610cbc34d3115b9840372efb2a18b86bbc78_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 27
        echo "
    ";
        
        $__internal_14e92c86302d5d412c22973712b0610cbc34d3115b9840372efb2a18b86bbc78->leave($__internal_14e92c86302d5d412c22973712b0610cbc34d3115b9840372efb2a18b86bbc78_prof);

    }

    public function getTemplateName()
    {
        return "EdebexAppBundle::layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  83 => 27,  77 => 26,  65 => 10,  56 => 29,  54 => 26,  35 => 10,  24 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html lang="en">*/
/* <head>*/
/*     <meta charset="utf-8">*/
/*     <meta http-equiv="X-UA-Compatible" content="IE=edge">*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1">*/
/*     <meta name="description" content="">*/
/*     <meta name="author" content="">*/
/* */
/*     <title>{% block title %}Edebex{% endblock %}</title>*/
/* */
/*     <!-- Bootstrap core CSS -->*/
/*     <link href="/css/bootstrap.min.css" rel="stylesheet">*/
/* */
/*     <!-- Animation CSS -->*/
/*     <link href="/css/animate.css" rel="stylesheet">*/
/*     <link href="/css/font-awesome.min.css" rel="stylesheet">*/
/* */
/*     <!-- Custom styles for this template -->*/
/*     <link href="/css/style.css" rel="stylesheet">*/
/*     */
/*     <link href="/css/plugins/jQueryUI/jquery-ui-1.10.4.custom.min.css" rel="stylesheet">*/
/* </head>*/
/* <body id="page-top" class="landing-page">*/
/* */
/* 	{% block body %}*/
/* */
/*     {% endblock %}*/
/* */
/* </body>*/
/* </html>*/

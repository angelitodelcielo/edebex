<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_ce916d5384e4ef56b04b40c737bcdc2359b16240e2f7ef14491a105734411aee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_210a7715f39b1f242f96e3c7e29984b1800e09d9b901aa761d878bf80ec126b1 = $this->env->getExtension("native_profiler");
        $__internal_210a7715f39b1f242f96e3c7e29984b1800e09d9b901aa761d878bf80ec126b1->enter($__internal_210a7715f39b1f242f96e3c7e29984b1800e09d9b901aa761d878bf80ec126b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_210a7715f39b1f242f96e3c7e29984b1800e09d9b901aa761d878bf80ec126b1->leave($__internal_210a7715f39b1f242f96e3c7e29984b1800e09d9b901aa761d878bf80ec126b1_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_8a1d4c30fcedf213cc7056866858791488796398bf402f9ad691640cdb694c92 = $this->env->getExtension("native_profiler");
        $__internal_8a1d4c30fcedf213cc7056866858791488796398bf402f9ad691640cdb694c92->enter($__internal_8a1d4c30fcedf213cc7056866858791488796398bf402f9ad691640cdb694c92_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_8a1d4c30fcedf213cc7056866858791488796398bf402f9ad691640cdb694c92->leave($__internal_8a1d4c30fcedf213cc7056866858791488796398bf402f9ad691640cdb694c92_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_80a629b6943ac104e4ac3798ddb0c4adf9ace93d1deb9f289db3259894017c29 = $this->env->getExtension("native_profiler");
        $__internal_80a629b6943ac104e4ac3798ddb0c4adf9ace93d1deb9f289db3259894017c29->enter($__internal_80a629b6943ac104e4ac3798ddb0c4adf9ace93d1deb9f289db3259894017c29_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_80a629b6943ac104e4ac3798ddb0c4adf9ace93d1deb9f289db3259894017c29->leave($__internal_80a629b6943ac104e4ac3798ddb0c4adf9ace93d1deb9f289db3259894017c29_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_84ae99e4c6172a06e9249f1fd2915e80abab1b304d90b8ba06f57bbd1d6f0beb = $this->env->getExtension("native_profiler");
        $__internal_84ae99e4c6172a06e9249f1fd2915e80abab1b304d90b8ba06f57bbd1d6f0beb->enter($__internal_84ae99e4c6172a06e9249f1fd2915e80abab1b304d90b8ba06f57bbd1d6f0beb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("TwigBundle:Exception:exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_84ae99e4c6172a06e9249f1fd2915e80abab1b304d90b8ba06f57bbd1d6f0beb->leave($__internal_84ae99e4c6172a06e9249f1fd2915e80abab1b304d90b8ba06f57bbd1d6f0beb_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'TwigBundle::layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include 'TwigBundle:Exception:exception.html.twig' %}*/
/* {% endblock %}*/
/* */

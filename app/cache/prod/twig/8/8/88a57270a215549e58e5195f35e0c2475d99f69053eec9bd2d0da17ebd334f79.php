<?php

/* EdebexAdminBundle:Admin:list.html.twig */
class __TwigTemplate_ef4630fb6a7642209a7b6509d1a356bc2e94d48f3117882c4a709f90bb197250 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("EdebexAdminBundle::layout.html.twig", "EdebexAdminBundle:Admin:list.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "EdebexAdminBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3878acd2571ce4a00b8aca45873dc455f88214455b05ce0b05794608063a1bc6 = $this->env->getExtension("native_profiler");
        $__internal_3878acd2571ce4a00b8aca45873dc455f88214455b05ce0b05794608063a1bc6->enter($__internal_3878acd2571ce4a00b8aca45873dc455f88214455b05ce0b05794608063a1bc6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EdebexAdminBundle:Admin:list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3878acd2571ce4a00b8aca45873dc455f88214455b05ce0b05794608063a1bc6->leave($__internal_3878acd2571ce4a00b8aca45873dc455f88214455b05ce0b05794608063a1bc6_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_6d2ca7b49910b410cecb9c9a4c94423cf804f34794a5672b369a72e54281ed2c = $this->env->getExtension("native_profiler");
        $__internal_6d2ca7b49910b410cecb9c9a4c94423cf804f34794a5672b369a72e54281ed2c->enter($__internal_6d2ca7b49910b410cecb9c9a4c94423cf804f34794a5672b369a72e54281ed2c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo " ";
        $this->displayParentBlock("title", $context, $blocks);
        echo " | Invoices ";
        
        $__internal_6d2ca7b49910b410cecb9c9a4c94423cf804f34794a5672b369a72e54281ed2c->leave($__internal_6d2ca7b49910b410cecb9c9a4c94423cf804f34794a5672b369a72e54281ed2c_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_85aa4a50f3cdbaf49a647ca152bd1d91889a63f992bfff2504bb684adb6b0efa = $this->env->getExtension("native_profiler");
        $__internal_85aa4a50f3cdbaf49a647ca152bd1d91889a63f992bfff2504bb684adb6b0efa->enter($__internal_85aa4a50f3cdbaf49a647ca152bd1d91889a63f992bfff2504bb684adb6b0efa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    <div id=\"wrapper\">
\t    <nav class=\"navbar-default navbar-static-side\" role=\"navigation\">
\t        <div class=\"sidebar-collapse\">
\t            <ul class=\"nav metismenu\" id=\"side-menu\">
\t                <li class=\"nav-header\">
\t                    <div class=\"dropdown profile-element\"> 
\t                    \t<span>
\t                            <img alt=\"image\" class=\"img-responsive\" src=\"/img/edebex-logo.png\" />
\t                        </span>
\t                        <ul class=\"dropdown-menu animated fadeInRight m-t-xs\">
\t                            <li><a href=\"profile.html\">Profile</a></li>
\t                            <li><a href=\"contacts.html\">Contacts</a></li>
\t                            <li><a href=\"mailbox.html\">Mailbox</a></li>
\t                            <li class=\"divider\"></li>
\t                            <li><a href=\"login.html\">Logout</a></li>
\t                        </ul>
\t                    </div>
\t                    <div class=\"logo-element\">
\t                        EDB
\t                    </div>
\t                </li>
\t                <li>
\t                    <a href=\"/admin/invoice\"><i class=\"fa fa-th-large\"></i> <span class=\"nav-label\">Invoices</span></a>
\t                </li>
\t            </ul>
\t        </div>
\t    </nav>

        <div id=\"page-wrapper\" class=\"gray-bg\">
\t        <div class=\"row border-bottom\">
\t\t        <nav class=\"navbar navbar-static-top\" role=\"navigation\" style=\"margin-bottom: 0\">
\t\t\t        <div class=\"navbar-header\">
\t\t\t            <a class=\"navbar-minimalize minimalize-styl-2 btn btn-primary \" href=\"#\"><i class=\"fa fa-bars\"></i> </a>
\t\t\t            <form role=\"search\" class=\"navbar-form-custom\" action=\"search_results.html\">
\t\t\t                <div class=\"form-group\">
\t\t\t                    <input type=\"text\" placeholder=\"Search for something...\" class=\"form-control\" name=\"top-search\" id=\"top-search\">
\t\t\t                </div>
\t\t\t            </form>
\t\t\t        </div>
\t\t            <ul class=\"nav navbar-top-links navbar-right\">
\t\t                <li>
\t\t                    <a href=\"login.html\">
\t\t                        <i class=\"fa fa-sign-out\"></i> Log out
\t\t                    </a>
\t\t                </li>
\t\t            </ul>
\t\t        </nav>
\t        </div>
\t\t        
            <div class=\"row wrapper border-bottom white-bg page-heading\">
                <div class=\"col-lg-10\">
                    <h2>Invoice List</h2>
                </div>
                <div class=\"col-lg-2\">

                </div>
            </div>
\t            
\t       \t<div class=\"wrapper wrapper-content animated fadeInRight\">
\t            <div class=\"row\">
\t                <div class=\"col-lg-12\">
\t                    <div class=\"ibox float-e-margins\">
\t                        <div class=\"ibox-content\">
\t                            <div class=\"table-responsive\">
\t                                <table class=\"table table-striped\">
\t                                    <thead>
\t\t                                    <tr>
\t\t                                        <th></th>
\t\t                                        <th>Reference</th>
\t\t                                        <th>Amount</th>
\t\t                                        <th>Due Date</th>
\t\t                                        <th>Status</th>
\t\t                                        <th>Action</th>
\t\t                                    </tr>
\t                                    </thead>
\t                                    <tbody>
\t                                    \t";
        // line 83
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["invoices"]) ? $context["invoices"] : $this->getContext($context, "invoices")));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["invoice"]) {
            echo "  
\t                                    \t<tr>    
\t\t                                        <td><input type=\"checkbox\"  class=\"i-checks\" name=\"input[]\"></td>
\t\t                                        <td><a href=\"";
            // line 86
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_single", array("reference" => $this->getAttribute($context["invoice"], "reference", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["invoice"], "reference", array()), "html", null, true);
            echo "</a></td>
\t\t                                        <td>";
            // line 87
            echo twig_escape_filter($this->env, $this->getAttribute($context["invoice"], "amount", array()), "html", null, true);
            echo " €</td>
\t\t                                        <td>";
            // line 88
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["invoice"], "duedate", array()), "m/d/Y"), "html", null, true);
            echo "</td>
\t\t                                        <td><span class=\"label \">
\t\t                                        \t";
            // line 90
            echo twig_escape_filter($this->env, $this->getAttribute($context["invoice"], "status", array()), "html", null, true);
            echo "</span></td>
\t\t                                        <td>
\t\t                                        \t";
            // line 92
            if (($this->getAttribute($context["invoice"], "status", array()) != "Approved")) {
                // line 93
                echo "\t\t                                        \t<form method=\"post\" action=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_edit", array("id" => $this->getAttribute($context["invoice"], "id", array()))), "html", null, true);
                echo "\">
  \t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"_method\" value=\"put\" />
\t\t                                        \t\t<button class=\"btn btn-primary btn-xs\" type=\"submit\" href=\"#\" class=\"approve\" data-value=\"";
                // line 95
                echo twig_escape_filter($this->env, $this->getAttribute($context["invoice"], "id", array()), "html", null, true);
                echo "\">
\t\t                                        \t\t<i class=\"fa fa-check\"></i>
\t\t                                        \t\t</button> 
\t\t                                        \t</form>
\t\t                                        \t";
            } else {
                // line 100
                echo "\t\t                                        \t\t";
                echo twig_escape_filter($this->env, (isset($context["tosend"]) ? $context["tosend"] : $this->getContext($context, "tosend")), "html", null, true);
                echo "
\t\t                                        \t";
            }
            // line 102
            echo "\t\t                                        </td>
\t\t                                    </tr>
\t\t                                    ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 105
            echo "\t\t                                    <tr>
\t\t\t\t\t\t\t\t\t\t\t\t<td colspan=\"6\" class=\"text-center\">No Invoice</td>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['invoice'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 109
        echo "\t                                    </tbody>
\t                                </table>
\t                            </div>
\t                        </div>
\t                    </div>
\t                </div>
\t            </div>
\t        </div>
\t        <div class=\"footer\">
\t            <div class=\"pull-right\">
\t                10GB of <strong>250GB</strong> Free.
\t            </div>
\t            <div>
\t                <strong>Copyright</strong> Example Company &copy; 2014-2015
\t            </div>
\t        </div>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src=\"/js/jquery-2.1.1.js\"></script>
    <script src=\"/js/bootstrap.min.js\"></script>
    <script src=\"/js/plugins/metisMenu/jquery.metisMenu.js\"></script>
    <script src=\"/js/plugins/slimscroll/jquery.slimscroll.min.js\"></script>

    <!-- Peity -->
    <script src=\"/js/plugins/peity/jquery.peity.min.js\"></script>

    <!-- Custom and plugin javascript -->
    <script src=\"/js/inspinia.js\"></script>
    <script src=\"/js/plugins/pace/pace.min.js\"></script>

    <!-- iCheck -->
    <script src=\"/js/plugins/iCheck/icheck.min.js\"></script>

    <!-- Peity -->
    <script src=\"/js/demo/peity-demo.js\"></script>

";
        
        $__internal_85aa4a50f3cdbaf49a647ca152bd1d91889a63f992bfff2504bb684adb6b0efa->leave($__internal_85aa4a50f3cdbaf49a647ca152bd1d91889a63f992bfff2504bb684adb6b0efa_prof);

    }

    public function getTemplateName()
    {
        return "EdebexAdminBundle:Admin:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  201 => 109,  192 => 105,  185 => 102,  179 => 100,  171 => 95,  165 => 93,  163 => 92,  158 => 90,  153 => 88,  149 => 87,  143 => 86,  134 => 83,  55 => 6,  49 => 5,  35 => 3,  11 => 1,);
    }
}
/* {% extends "EdebexAdminBundle::layout.html.twig" %}*/
/* */
/* {% block title %} {{ parent() }} | Invoices {% endblock %}*/
/* */
/* {% block body %}*/
/* */
/*     <div id="wrapper">*/
/* 	    <nav class="navbar-default navbar-static-side" role="navigation">*/
/* 	        <div class="sidebar-collapse">*/
/* 	            <ul class="nav metismenu" id="side-menu">*/
/* 	                <li class="nav-header">*/
/* 	                    <div class="dropdown profile-element"> */
/* 	                    	<span>*/
/* 	                            <img alt="image" class="img-responsive" src="/img/edebex-logo.png" />*/
/* 	                        </span>*/
/* 	                        <ul class="dropdown-menu animated fadeInRight m-t-xs">*/
/* 	                            <li><a href="profile.html">Profile</a></li>*/
/* 	                            <li><a href="contacts.html">Contacts</a></li>*/
/* 	                            <li><a href="mailbox.html">Mailbox</a></li>*/
/* 	                            <li class="divider"></li>*/
/* 	                            <li><a href="login.html">Logout</a></li>*/
/* 	                        </ul>*/
/* 	                    </div>*/
/* 	                    <div class="logo-element">*/
/* 	                        EDB*/
/* 	                    </div>*/
/* 	                </li>*/
/* 	                <li>*/
/* 	                    <a href="/admin/invoice"><i class="fa fa-th-large"></i> <span class="nav-label">Invoices</span></a>*/
/* 	                </li>*/
/* 	            </ul>*/
/* 	        </div>*/
/* 	    </nav>*/
/* */
/*         <div id="page-wrapper" class="gray-bg">*/
/* 	        <div class="row border-bottom">*/
/* 		        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">*/
/* 			        <div class="navbar-header">*/
/* 			            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>*/
/* 			            <form role="search" class="navbar-form-custom" action="search_results.html">*/
/* 			                <div class="form-group">*/
/* 			                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">*/
/* 			                </div>*/
/* 			            </form>*/
/* 			        </div>*/
/* 		            <ul class="nav navbar-top-links navbar-right">*/
/* 		                <li>*/
/* 		                    <a href="login.html">*/
/* 		                        <i class="fa fa-sign-out"></i> Log out*/
/* 		                    </a>*/
/* 		                </li>*/
/* 		            </ul>*/
/* 		        </nav>*/
/* 	        </div>*/
/* 		        */
/*             <div class="row wrapper border-bottom white-bg page-heading">*/
/*                 <div class="col-lg-10">*/
/*                     <h2>Invoice List</h2>*/
/*                 </div>*/
/*                 <div class="col-lg-2">*/
/* */
/*                 </div>*/
/*             </div>*/
/* 	            */
/* 	       	<div class="wrapper wrapper-content animated fadeInRight">*/
/* 	            <div class="row">*/
/* 	                <div class="col-lg-12">*/
/* 	                    <div class="ibox float-e-margins">*/
/* 	                        <div class="ibox-content">*/
/* 	                            <div class="table-responsive">*/
/* 	                                <table class="table table-striped">*/
/* 	                                    <thead>*/
/* 		                                    <tr>*/
/* 		                                        <th></th>*/
/* 		                                        <th>Reference</th>*/
/* 		                                        <th>Amount</th>*/
/* 		                                        <th>Due Date</th>*/
/* 		                                        <th>Status</th>*/
/* 		                                        <th>Action</th>*/
/* 		                                    </tr>*/
/* 	                                    </thead>*/
/* 	                                    <tbody>*/
/* 	                                    	{% for invoice in invoices %}  */
/* 	                                    	<tr>    */
/* 		                                        <td><input type="checkbox"  class="i-checks" name="input[]"></td>*/
/* 		                                        <td><a href="{{ path('admin_single', { 'reference': invoice.reference }) }}">{{invoice.reference}}</a></td>*/
/* 		                                        <td>{{invoice.amount}} €</td>*/
/* 		                                        <td>{{invoice.duedate | date("m/d/Y")}}</td>*/
/* 		                                        <td><span class="label ">*/
/* 		                                        	{{invoice.status}}</span></td>*/
/* 		                                        <td>*/
/* 		                                        	{% if invoice.status != "Approved" %}*/
/* 		                                        	<form method="post" action="{{ path('admin_edit', {'id': invoice.id }) }}">*/
/*   														<input type="hidden" name="_method" value="put" />*/
/* 		                                        		<button class="btn btn-primary btn-xs" type="submit" href="#" class="approve" data-value="{{invoice.id}}">*/
/* 		                                        		<i class="fa fa-check"></i>*/
/* 		                                        		</button> */
/* 		                                        	</form>*/
/* 		                                        	{% else %}*/
/* 		                                        		{{tosend}}*/
/* 		                                        	{% endif %}*/
/* 		                                        </td>*/
/* 		                                    </tr>*/
/* 		                                    {% else %}*/
/* 		                                    <tr>*/
/* 												<td colspan="6" class="text-center">No Invoice</td>*/
/* 											</tr>*/
/* 		                                    {% endfor %}*/
/* 	                                    </tbody>*/
/* 	                                </table>*/
/* 	                            </div>*/
/* 	                        </div>*/
/* 	                    </div>*/
/* 	                </div>*/
/* 	            </div>*/
/* 	        </div>*/
/* 	        <div class="footer">*/
/* 	            <div class="pull-right">*/
/* 	                10GB of <strong>250GB</strong> Free.*/
/* 	            </div>*/
/* 	            <div>*/
/* 	                <strong>Copyright</strong> Example Company &copy; 2014-2015*/
/* 	            </div>*/
/* 	        </div>*/
/*         </div>*/
/*     </div>*/
/* */
/*     <!-- Mainly scripts -->*/
/*     <script src="/js/jquery-2.1.1.js"></script>*/
/*     <script src="/js/bootstrap.min.js"></script>*/
/*     <script src="/js/plugins/metisMenu/jquery.metisMenu.js"></script>*/
/*     <script src="/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>*/
/* */
/*     <!-- Peity -->*/
/*     <script src="/js/plugins/peity/jquery.peity.min.js"></script>*/
/* */
/*     <!-- Custom and plugin javascript -->*/
/*     <script src="/js/inspinia.js"></script>*/
/*     <script src="/js/plugins/pace/pace.min.js"></script>*/
/* */
/*     <!-- iCheck -->*/
/*     <script src="/js/plugins/iCheck/icheck.min.js"></script>*/
/* */
/*     <!-- Peity -->*/
/*     <script src="/js/demo/peity-demo.js"></script>*/
/* */
/* {% endblock %}*/

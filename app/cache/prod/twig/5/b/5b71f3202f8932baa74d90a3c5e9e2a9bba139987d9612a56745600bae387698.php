<?php

/* EdebexAppBundle:Invoice:index.html.twig */
class __TwigTemplate_d5bd324d9caf6448975282b5f24654fd91ab58d38fd8ec997b5daf8bf6889d73 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("EdebexAppBundle::layout.html.twig", "EdebexAppBundle:Invoice:index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "EdebexAppBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b447aab02d1517c14ce0499f59db12da67b4a43ef47da759535113f968ebda38 = $this->env->getExtension("native_profiler");
        $__internal_b447aab02d1517c14ce0499f59db12da67b4a43ef47da759535113f968ebda38->enter($__internal_b447aab02d1517c14ce0499f59db12da67b4a43ef47da759535113f968ebda38_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EdebexAppBundle:Invoice:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b447aab02d1517c14ce0499f59db12da67b4a43ef47da759535113f968ebda38->leave($__internal_b447aab02d1517c14ce0499f59db12da67b4a43ef47da759535113f968ebda38_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_171ba5b510d51fb3ad9140beb1f2a112219746ce0618affa6ade775034ae26f6 = $this->env->getExtension("native_profiler");
        $__internal_171ba5b510d51fb3ad9140beb1f2a112219746ce0618affa6ade775034ae26f6->enter($__internal_171ba5b510d51fb3ad9140beb1f2a112219746ce0618affa6ade775034ae26f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo " ";
        $this->displayParentBlock("title", $context, $blocks);
        echo " | Easy Invoice Maker ";
        
        $__internal_171ba5b510d51fb3ad9140beb1f2a112219746ce0618affa6ade775034ae26f6->leave($__internal_171ba5b510d51fb3ad9140beb1f2a112219746ce0618affa6ade775034ae26f6_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_44549faed12737357a8f018d46e8e06b28148b60643df054cff02244435be542 = $this->env->getExtension("native_profiler");
        $__internal_44549faed12737357a8f018d46e8e06b28148b60643df054cff02244435be542->enter($__internal_44549faed12737357a8f018d46e8e06b28148b60643df054cff02244435be542_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
\t<div class=\"navbar-wrapper\">
\t        <nav class=\"navbar navbar-default navbar-fixed-top\" role=\"navigation\">
\t            <div class=\"container\">
\t                <div class=\"navbar-header page-scroll\">
\t                    <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">
\t                        <span class=\"sr-only\">Toggle navigation</span>
\t                        <span class=\"icon-bar\"></span>
\t                        <span class=\"icon-bar\"></span>
\t                        <span class=\"icon-bar\"></span>
\t                    </button>
\t                    <a href=\"#\"><img src=\"/img/edebex-logo.png\" alt=\"\" width=\"200\"></a>
\t                </div>
\t                <div id=\"navbar\" class=\"navbar-collapse collapse\">
\t                    <ul class=\"nav navbar-nav navbar-right\">
\t                        <li><a class=\"page-scroll\" href=\"/\">Home</a></li>
\t                        <li><a class=\"page-scroll\" href=\"login\">login</a></li>
\t                    </ul>
\t                </div>
\t            </div>
\t        </nav>
\t</div>
\t<div id=\"inSlider\" class=\"carousel slide carousel-fade\" data-ride=\"carousel\">
\t    <ol class=\"carousel-indicators\">
\t        <li data-target=\"#inSlider\" data-slide-to=\"0\" class=\"active\"></li>
\t        <li data-target=\"#inSlider\" data-slide-to=\"1\"></li>
\t    </ol>
\t    <div class=\"carousel-inner\" role=\"listbox\">
\t        <div class=\"item active\">
\t            <div class=\"container\">
\t                <div class=\"carousel-caption blank\">
\t                    <h1>Let's make your invoicing <br/> simple and easy.</h1>
\t                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p>
\t                    <p><a class=\"btn btn-lg btn-primary\" href=\"#\" role=\"button\">Learn more</a></p>
\t                </div>
\t            </div>
\t            <!-- Set background for slide in css -->
\t            <div class=\"header-back two\"></div>
\t        </div>
\t    </div>
\t</div>
\t
\t<section id=\"contact\">
\t    <div class=\"container\">
\t\t    <div class=\"row m-b-lg\">
\t            <div class=\"col-lg-12 text-center\">
\t                <div class=\"navy-line\"></div>
\t                <h1>Invoice Maker</h1>
\t                <p>Please fill the following form to send us a demand of invoicing.</p>
\t            </div>
\t        </div>
\t        <div class=\"row m-b-lg\">
\t            <div class=\"col-lg-12 text-center\">
\t            
\t\t\t\t\t  ";
        // line 60
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 61
            echo "  \t\t\t\t\t    <div class=\"alert alert-success\">
\t\t\t\t\t        ";
            // line 62
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "
\t\t\t\t\t    </div>
\t                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 65
        echo "\t\t\t\t\t\t
                      ";
        // line 66
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("attr" => array("class" => "form-horizontal")));
        echo "
\t\t\t\t\t  ";
        // line 67
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
\t\t\t\t\t
\t\t\t\t\t  <div class=\"form-group\">
\t\t\t\t\t      ";
        // line 70
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "duedate", array()), 'label', array("label_attr" => array("class" => "col-sm-3 control-label"), "label" => "Due Date"));
        echo "

\t\t\t\t\t      <div class=\"col-sm-4\" id=\"datapicker\">
\t\t\t\t\t      \t<div class=\"input-group date\">
                                  <span class=\"input-group-addon\"><i class=\"fa fa-calendar\"></i></span>
                                  ";
        // line 75
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "duedate", array()), 'widget', array("attr" => array("class" => "form-control date")));
        echo "
                            </div>
\t\t\t\t\t      </div>
\t\t\t\t\t      <div class=\"col-sm-4\">
\t\t\t\t\t     \t ";
        // line 79
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "duedate", array()), 'errors');
        echo "
\t\t\t\t\t      </div>
\t\t\t\t\t  </div>
\t\t\t\t\t    
\t\t\t\t\t  <div class=\"form-group\">
\t\t\t\t\t      ";
        // line 84
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "amount", array()), 'label', array("label_attr" => array("class" => "col-sm-3 control-label"), "label" => "Amount"));
        echo "
\t\t\t\t\t      
\t\t\t\t\t      <div class=\"col-sm-4\">
\t\t\t\t\t      \t<div class=\"input-group date\">
                                  <span class=\"input-group-addon\"><i class=\"fa fa-euro\"></i></span>
\t\t\t\t        \t\t";
        // line 89
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "amount", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t\t\t\t        \t</div>
\t\t\t\t\t      </div>
\t\t\t\t\t      <div class=\"col-sm-4\">
\t\t\t\t\t     \t ";
        // line 93
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "amount", array()), 'errors');
        echo "
\t\t\t\t\t      </div>
\t\t\t\t\t  </div>
\t\t\t\t\t    
\t\t\t\t\t  <div class=\"form-group\">
\t\t\t\t\t      ";
        // line 98
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "reference", array()), 'label', array("label_attr" => array("class" => "col-sm-3 control-label"), "label" => "Customer’s invoice reference"));
        echo "
\t\t\t\t\t      
\t\t\t\t\t      <div class=\"col-sm-4\">
\t\t\t\t\t        ";
        // line 101
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "reference", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t\t\t\t\t      </div>
\t\t\t\t\t      <div class=\"col-sm-4\">
\t\t\t\t\t     \t ";
        // line 104
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "reference", array()), 'errors');
        echo "
\t\t\t\t\t      </div>
\t\t\t\t\t  </div>
\t\t\t\t\t    
\t\t\t\t\t  <div class=\"form-group\">
\t\t\t\t\t      ";
        // line 109
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "emailInvoicing", array()), 'label', array("label_attr" => array("class" => "col-sm-3 control-label"), "label" => "Invoicing company email"));
        echo "
\t\t\t\t\t      
\t\t\t\t\t      <div class=\"col-sm-4\">
\t\t\t\t\t        ";
        // line 112
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "emailInvoicing", array()), 'widget', array("attr" => array("class" => "form-control emailinvoicing", "id" => "emailinvoicing")));
        echo "
\t\t\t\t\t      </div>
\t\t\t\t\t      <div class=\"col-sm-4\">
\t\t\t\t\t     \t ";
        // line 115
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "emailInvoicing", array()), 'errors');
        echo "
\t\t\t\t\t      </div>
\t\t\t\t\t  </div>
\t\t\t\t\t    
\t\t\t\t\t  <div class=\"form-group\">
\t\t\t\t\t      ";
        // line 120
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "emailInvoiced", array()), 'label', array("label_attr" => array("class" => "col-sm-3 control-label"), "label" => "Invoiced company email"));
        echo "
\t\t\t\t\t      
\t\t\t\t\t      <div class=\"col-sm-4\">
\t\t\t\t\t        ";
        // line 123
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "emailInvoiced", array()), 'widget', array("attr" => array("class" => "form-control", "id" => "emailinvoiced")));
        echo "
\t\t\t\t\t      </div>
\t\t\t\t\t      <div class=\"col-sm-4\">
\t\t\t\t\t     \t ";
        // line 126
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "emailInvoiced", array()), 'errors');
        echo "
\t\t\t\t\t      </div>
\t\t\t\t\t  </div>
\t\t\t\t\t
\t\t\t\t\t  ";
        // line 130
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "save", array()), 'widget', array("attr" => array("class" => "btn btn-primary")));
        echo "
\t\t\t\t\t  ";
        // line 131
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "
\t\t\t\t\t  ";
        // line 132
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
\t            </div>
\t        </div>        
\t    </div>
\t</section>
\t<section id=\"contact\" class=\"dark-bg\"> 
\t    <div class=\"container\">
\t    \t    <div class=\"row\">
\t            <div class=\"col-lg-8 col-lg-offset-2 text-center m-t-lg\">
\t                <p><strong>&copy; 2015 Edebex</strong><br/></p>
\t            </div>
\t        </div>
\t        <div class=\"row m-b-lg\">
\t            <div class=\"col-lg-12 text-center\">
\t            \t<p class=\"m-t-sm\">
\t                    Follow us on our social platform
\t                </p>
\t                <ul class=\"list-inline social-icon\">
\t                    <li><a href=\"#\"><i class=\"fa fa-twitter\"></i></a>
\t                    </li>
\t                    <li><a href=\"#\"><i class=\"fa fa-facebook\"></i></a>
\t                    </li>
\t                    <li><a href=\"#\"><i class=\"fa fa-linkedin\"></i></a>
\t                    </li>
\t                </ul>
\t            </div>
\t\t    </div>
\t
\t    </div>
\t</section>
\t
\t<!-- Mainly scripts -->
    <script src=\"/js/jquery-2.1.1.js\"></script>
    <script src=\"/js/bootstrap.min.js\"></script>
    
    <script src=\"/js/jquery-ui-1.10.4.min.js\">
    
\t<!-- MENU -->
    <script src=\"/js/plugins/metisMenu/jquery.metisMenu.js\"></script>

    <!-- Custom and plugin javascript -->
    <script src=\"/js/inspinia.js\"></script>
    <script src=\"/js/plugins/pace/pace.min.js\"></script>
    <script src=\"/js/plugins/slimscroll/jquery.slimscroll.min.js\"></script>
    
    <!-- Datepicker -->
\t<script src=\"/js/plugins/datapicker/bootstrap-datepicker.js\"></script>

\t<style>
\t.ui-autocomplete {
\t    position: absolute;
\t}
\t</style>
    
\t<script>
\t\t\$('#datapicker .input-group.date').datepicker({
            todayBtn: \"linked\",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true
        });
        
        var availableTags = [\"seller@seller.foo\",\"debtor@debtor.foo\"];
        
       \$( \"#form_emailInvoicing, #form_emailInvoiced\" ).autocomplete({
\t        source: availableTags,
\t        minLength: 3,
\t        messages: {
\t\t        noResults: '',
\t\t        results: function() {}
\t\t    }
\t   });
    </script>
\t

";
        
        $__internal_44549faed12737357a8f018d46e8e06b28148b60643df054cff02244435be542->leave($__internal_44549faed12737357a8f018d46e8e06b28148b60643df054cff02244435be542_prof);

    }

    public function getTemplateName()
    {
        return "EdebexAppBundle:Invoice:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  253 => 132,  249 => 131,  245 => 130,  238 => 126,  232 => 123,  226 => 120,  218 => 115,  212 => 112,  206 => 109,  198 => 104,  192 => 101,  186 => 98,  178 => 93,  171 => 89,  163 => 84,  155 => 79,  148 => 75,  140 => 70,  134 => 67,  130 => 66,  127 => 65,  118 => 62,  115 => 61,  111 => 60,  55 => 6,  49 => 5,  35 => 3,  11 => 1,);
    }
}
/* {% extends "EdebexAppBundle::layout.html.twig" %}*/
/* */
/* {% block title %} {{ parent() }} | Easy Invoice Maker {% endblock %}*/
/* */
/* {% block body %}*/
/* */
/* 	<div class="navbar-wrapper">*/
/* 	        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">*/
/* 	            <div class="container">*/
/* 	                <div class="navbar-header page-scroll">*/
/* 	                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">*/
/* 	                        <span class="sr-only">Toggle navigation</span>*/
/* 	                        <span class="icon-bar"></span>*/
/* 	                        <span class="icon-bar"></span>*/
/* 	                        <span class="icon-bar"></span>*/
/* 	                    </button>*/
/* 	                    <a href="#"><img src="/img/edebex-logo.png" alt="" width="200"></a>*/
/* 	                </div>*/
/* 	                <div id="navbar" class="navbar-collapse collapse">*/
/* 	                    <ul class="nav navbar-nav navbar-right">*/
/* 	                        <li><a class="page-scroll" href="/">Home</a></li>*/
/* 	                        <li><a class="page-scroll" href="login">login</a></li>*/
/* 	                    </ul>*/
/* 	                </div>*/
/* 	            </div>*/
/* 	        </nav>*/
/* 	</div>*/
/* 	<div id="inSlider" class="carousel slide carousel-fade" data-ride="carousel">*/
/* 	    <ol class="carousel-indicators">*/
/* 	        <li data-target="#inSlider" data-slide-to="0" class="active"></li>*/
/* 	        <li data-target="#inSlider" data-slide-to="1"></li>*/
/* 	    </ol>*/
/* 	    <div class="carousel-inner" role="listbox">*/
/* 	        <div class="item active">*/
/* 	            <div class="container">*/
/* 	                <div class="carousel-caption blank">*/
/* 	                    <h1>Let's make your invoicing <br/> simple and easy.</h1>*/
/* 	                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p>*/
/* 	                    <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>*/
/* 	                </div>*/
/* 	            </div>*/
/* 	            <!-- Set background for slide in css -->*/
/* 	            <div class="header-back two"></div>*/
/* 	        </div>*/
/* 	    </div>*/
/* 	</div>*/
/* 	*/
/* 	<section id="contact">*/
/* 	    <div class="container">*/
/* 		    <div class="row m-b-lg">*/
/* 	            <div class="col-lg-12 text-center">*/
/* 	                <div class="navy-line"></div>*/
/* 	                <h1>Invoice Maker</h1>*/
/* 	                <p>Please fill the following form to send us a demand of invoicing.</p>*/
/* 	            </div>*/
/* 	        </div>*/
/* 	        <div class="row m-b-lg">*/
/* 	            <div class="col-lg-12 text-center">*/
/* 	            */
/* 					  {% for flashMessage in app.session.flashbag.get('notice') %}*/
/*   					    <div class="alert alert-success">*/
/* 					        {{ flashMessage }}*/
/* 					    </div>*/
/* 	                  {% endfor %}*/
/* 						*/
/*                       {{ form_start(form, {'attr': {'class': 'form-horizontal'}}) }}*/
/* 					  {{ form_errors(form) }}*/
/* 					*/
/* 					  <div class="form-group">*/
/* 					      {{ form_label(form.duedate, "Due Date", {'label_attr': {'class': 'col-sm-3 control-label'}}) }}*/
/* */
/* 					      <div class="col-sm-4" id="datapicker">*/
/* 					      	<div class="input-group date">*/
/*                                   <span class="input-group-addon"><i class="fa fa-calendar"></i></span>*/
/*                                   {{ form_widget(form.duedate, {'attr': {'class': 'form-control date'}}) }}*/
/*                             </div>*/
/* 					      </div>*/
/* 					      <div class="col-sm-4">*/
/* 					     	 {{ form_errors(form.duedate) }}*/
/* 					      </div>*/
/* 					  </div>*/
/* 					    */
/* 					  <div class="form-group">*/
/* 					      {{ form_label(form.amount, "Amount", {'label_attr': {'class': 'col-sm-3 control-label'}}) }}*/
/* 					      */
/* 					      <div class="col-sm-4">*/
/* 					      	<div class="input-group date">*/
/*                                   <span class="input-group-addon"><i class="fa fa-euro"></i></span>*/
/* 				        		{{ form_widget(form.amount, {'attr': {'class': 'form-control'}}) }}*/
/* 				        	</div>*/
/* 					      </div>*/
/* 					      <div class="col-sm-4">*/
/* 					     	 {{ form_errors(form.amount) }}*/
/* 					      </div>*/
/* 					  </div>*/
/* 					    */
/* 					  <div class="form-group">*/
/* 					      {{ form_label(form.reference, "Customer’s invoice reference", {'label_attr': {'class': 'col-sm-3 control-label'}}) }}*/
/* 					      */
/* 					      <div class="col-sm-4">*/
/* 					        {{ form_widget(form.reference, {'attr': {'class': 'form-control'}}) }}*/
/* 					      </div>*/
/* 					      <div class="col-sm-4">*/
/* 					     	 {{ form_errors(form.reference) }}*/
/* 					      </div>*/
/* 					  </div>*/
/* 					    */
/* 					  <div class="form-group">*/
/* 					      {{ form_label(form.emailInvoicing, "Invoicing company email", {'label_attr': {'class': 'col-sm-3 control-label'}}) }}*/
/* 					      */
/* 					      <div class="col-sm-4">*/
/* 					        {{ form_widget(form.emailInvoicing, {'attr': {'class': 'form-control emailinvoicing', 'id': 'emailinvoicing'}}) }}*/
/* 					      </div>*/
/* 					      <div class="col-sm-4">*/
/* 					     	 {{ form_errors(form.emailInvoicing) }}*/
/* 					      </div>*/
/* 					  </div>*/
/* 					    */
/* 					  <div class="form-group">*/
/* 					      {{ form_label(form.emailInvoiced, "Invoiced company email", {'label_attr': {'class': 'col-sm-3 control-label'}}) }}*/
/* 					      */
/* 					      <div class="col-sm-4">*/
/* 					        {{ form_widget(form.emailInvoiced, {'attr': {'class': 'form-control', 'id': 'emailinvoiced'}}) }}*/
/* 					      </div>*/
/* 					      <div class="col-sm-4">*/
/* 					     	 {{ form_errors(form.emailInvoiced) }}*/
/* 					      </div>*/
/* 					  </div>*/
/* 					*/
/* 					  {{ form_widget(form.save, {'attr': {'class': 'btn btn-primary'}}) }}*/
/* 					  {{ form_rest(form) }}*/
/* 					  {{ form_end(form) }}*/
/* 	            </div>*/
/* 	        </div>        */
/* 	    </div>*/
/* 	</section>*/
/* 	<section id="contact" class="dark-bg"> */
/* 	    <div class="container">*/
/* 	    	    <div class="row">*/
/* 	            <div class="col-lg-8 col-lg-offset-2 text-center m-t-lg">*/
/* 	                <p><strong>&copy; 2015 Edebex</strong><br/></p>*/
/* 	            </div>*/
/* 	        </div>*/
/* 	        <div class="row m-b-lg">*/
/* 	            <div class="col-lg-12 text-center">*/
/* 	            	<p class="m-t-sm">*/
/* 	                    Follow us on our social platform*/
/* 	                </p>*/
/* 	                <ul class="list-inline social-icon">*/
/* 	                    <li><a href="#"><i class="fa fa-twitter"></i></a>*/
/* 	                    </li>*/
/* 	                    <li><a href="#"><i class="fa fa-facebook"></i></a>*/
/* 	                    </li>*/
/* 	                    <li><a href="#"><i class="fa fa-linkedin"></i></a>*/
/* 	                    </li>*/
/* 	                </ul>*/
/* 	            </div>*/
/* 		    </div>*/
/* 	*/
/* 	    </div>*/
/* 	</section>*/
/* 	*/
/* 	<!-- Mainly scripts -->*/
/*     <script src="/js/jquery-2.1.1.js"></script>*/
/*     <script src="/js/bootstrap.min.js"></script>*/
/*     */
/*     <script src="/js/jquery-ui-1.10.4.min.js">*/
/*     */
/* 	<!-- MENU -->*/
/*     <script src="/js/plugins/metisMenu/jquery.metisMenu.js"></script>*/
/* */
/*     <!-- Custom and plugin javascript -->*/
/*     <script src="/js/inspinia.js"></script>*/
/*     <script src="/js/plugins/pace/pace.min.js"></script>*/
/*     <script src="/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>*/
/*     */
/*     <!-- Datepicker -->*/
/* 	<script src="/js/plugins/datapicker/bootstrap-datepicker.js"></script>*/
/* */
/* 	<style>*/
/* 	.ui-autocomplete {*/
/* 	    position: absolute;*/
/* 	}*/
/* 	</style>*/
/*     */
/* 	<script>*/
/* 		$('#datapicker .input-group.date').datepicker({*/
/*             todayBtn: "linked",*/
/*             keyboardNavigation: false,*/
/*             forceParse: false,*/
/*             calendarWeeks: true,*/
/*             autoclose: true*/
/*         });*/
/*         */
/*         var availableTags = ["seller@seller.foo","debtor@debtor.foo"];*/
/*         */
/*        $( "#form_emailInvoicing, #form_emailInvoiced" ).autocomplete({*/
/* 	        source: availableTags,*/
/* 	        minLength: 3,*/
/* 	        messages: {*/
/* 		        noResults: '',*/
/* 		        results: function() {}*/
/* 		    }*/
/* 	   });*/
/*     </script>*/
/* 	*/
/* */
/* {% endblock %}*/
/* */

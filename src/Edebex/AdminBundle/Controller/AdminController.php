<?php

namespace Edebex\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Library\BusinessCalculator;

class AdminController extends Controller
{	
    public function indexAction()
    {
    	$invoices = $this->getDoctrine()   	
    	->getManager()   	
    	->getRepository('EdebexAppBundle:Invoice')->findAll();
    	
    	//$sendtime = new BusinessCalculator();

        return $this->render('EdebexAdminBundle:Admin:list.html.twig', array('invoices' => $invoices, 'tosend' => ""));
    }
    
    public function showAction($reference)
    {
    	$repository = $this->getDoctrine()   	
    	->getManager()   	
    	->getRepository('EdebexAppBundle:Invoice');
    	
    	$invoice = $repository->findOneBy(array('reference' => $reference));

    	if (null === $invoice) {
    	
    		throw new NotFoundHttpException("Invoice n".$reference." doesn't exist.");
    	}
    	
    	return $this->render('EdebexAdminBundle:Admin:single.html.twig', array('invoice' => $invoice));
    }
    
    public function editAction($id)
    {
    	$em = $this->getDoctrine()->getEntityManager();
    
    	$invoice = $em->getRepository('EdebexAppBundle:Invoice')->find($id);
    	
    	$invoice->setStatus("Approved");
    
    	if (!$invoice) {
    		throw $this->createNotFoundException('Unable to find Invoice.');
    	}
    	
    	$em->flush();

    	return $this->redirect($this->generateUrl('admin_list')); 
    }
}

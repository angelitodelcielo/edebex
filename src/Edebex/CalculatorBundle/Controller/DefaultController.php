<?php

namespace Edebex\CalculatorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Library\BusinessCalculator;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
    	$date = $request->query->get('date', '2015-01-09 14:00:00');
    	
    	$obj = new BusinessCalculator($date);
    	echo "<hr>";
    	echo "<b>Message will be sent @".$obj -> getSendTime()."</b>";
    	
        return $this->render('EdebexCalculatorBundle:Default:index.html.twig');
    }
}

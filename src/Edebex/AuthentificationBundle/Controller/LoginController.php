<?php

namespace Edebex\AuthentificationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class LoginController extends Controller
{
    public function indexAction()
    {
    	$authenticationUtils = $this->get('security.authentication_utils');
    	
    	$error = $authenticationUtils->getLastAuthenticationError();
    	
    	$lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('EdebexAuthentificationBundle:Default:index.html.twig',
        	array(
    			'last_username' => $lastUsername,
    			'error'         => $error,
    		)
        );
    }
}

<?php

namespace Edebex\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Invoice
 *
 * @ORM\Table()
 * @ORM\Entity
 */ 
class Invoice
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255)
     */
    private $reference;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal")
     */
    private $amount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="due_date", type="datetime")
     */
    private $dueDate;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=40)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="approved_date", type="datetime",nullable=true)
     */
    private $approvedDate;

    /**
     * @var string
     *
     * @ORM\Column(name="email_invoicing", type="string", length=40)
     */
    private $emailInvoicing;

    /**
     * @var string
     *
     * @ORM\Column(name="email_invoiced", type="string", length=40)
     */
    private $emailInvoiced;
    
    public function __construct()
    {
    
    	$this->status = "Pending";
    
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return invoice
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return invoice
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set dueDate
     *
     * @param \DateTime $dueDate
     *
     * @return invoice
     */
    public function setDueDate($dueDate)
    {
        $this->dueDate = $dueDate;

        return $this;
    }

    /**
     * Get dueDate
     *
     * @return \DateTime
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return invoice
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set approvedDate
     *
     * @param \DateTime $approvedDate
     *
     * @return invoice
     */
    public function setApprovedDate($approvedDate)
    {
        $this->approvedDate = $approvedDate;

        return $this;
    }

    /**
     * Get approvedDate
     *
     * @return \DateTime
     */
    public function getApprovedDate()
    {
        return $this->approvedDate;
    }

    /**
     * Set emailInvoicing
     *
     * @param string $emailInvoicing
     *
     * @return invoice
     */
    public function setEmailInvoicing($emailInvoicing)
    {
        $this->emailInvoicing = $emailInvoicing;

        return $this;
    }

    /**
     * Get emailInvoicing
     *
     * @return string
     */
    public function getEmailInvoicing()
    {
        return $this->emailInvoicing;
    }

    /**
     * Set emailInvoiced
     *
     * @param string $emailInvoiced
     *
     * @return invoice
     */
    public function setEmailInvoiced($emailInvoiced)
    {
        $this->emailInvoiced = $emailInvoiced;

        return $this;
    }

    /**
     * Get emailInvoiced
     *
     * @return string
     */
    public function getEmailInvoiced()
    {
        return $this->emailInvoiced;
    }
}


<?php

namespace Library;

class BusinessCalculator{

    private $datetime = null;
    private $debug = true;
    private $businessTimeInterval = null;
    private $format = "Y-m-d H:i:s";
    private $countdown = 60 * 4 ;
 
    public function __construct($time){
    	
    	if(!$this -> validateDate($time)){
    		echo "INVALIDE FORMAT DATE: accepted => " . $this -> format;
    		die();
    	}

        $this -> datetime = new \DateTime($time);
        
        $days = $this -> datetime->format("Y-m-d");
        
        $this -> businessTimeInterval = array(
        	'am' => array('begin' => $days . ' 09:00:00', 'end' => $days . ' 12:00:00'),
        	'pm' => array('begin' => $days . ' 13:30:00', 'end' => $days . ' 17:00:00')
        );
        
        $this -> businessTimeBegin["am"] = new \DateTime($this->businessTimeInterval["am"]['begin']);
        $this -> businessTimeEnd["am"] = new \DateTime($this->businessTimeInterval["am"]['end']);
        
        $this -> businessTimeBegin["pm"] = new \DateTime($this->businessTimeInterval["pm"]['begin']);
        $this -> businessTimeEnd["pm"] = new \DateTime($this->businessTimeInterval["pm"]['end']);

        if($this -> debug) echo "### Approved at " . $this -> datetime -> format($this->format)." ###<br>";
        
        $this -> calculateSendTime();  
    }
    
    public function getSendTime(){
    	
    	return $this -> datetime -> format($this->format);
    }

    private function calculateSendTime()
    {
    	include_once $_SERVER["DOCUMENT_ROOT"]. '/../vendor/ics-parser/ICal.class.php';
    	$ical   = new \ICal($_SERVER["DOCUMENT_ROOT"].'/../app/Resources/ics_2015_belgium.ics');
    	$events = $ical->events();
    	
		$complete = false;
		while(!$complete){
			// Check particular case, we can include satuday and sunday too
			$isMonday = $this -> datetime ->format('l') == 'Monday';
			$isFriday = $this -> datetime ->format('l') == 'Friday';
			
			$isSaturday = $this -> datetime ->format('l') == 'Saturday';
			$isSunday = $this -> datetime ->format('l') == 'Sunday';
			// display the current day
			if($this->debug) echo "<br>".$this -> datetime ->format('l') . " - [ day: ".$this -> datetime -> format($this->format)." ]<br>";
			// Check if holiday in ics
			$isHoliday = false;
			foreach ($events as $event) {
				// 2015 events	
				if($event['DTSTART'] > "20150000"){
					// Holiday ?
					if($event['DTSTART'] == $this -> datetime ->format("Ymd")){
						
						$isHoliday = true;
						break;
					}
				}
			}
			
			if(!$isHoliday and ! $isSaturday and !$isSunday){
				
				if($isMonday){
					// Monday only after midday
					$this -> calculateBusinessTime("pm");
				}
				elseif($isFriday){
					// Friday only before midday
					$this -> calculateBusinessTime("am");
				}
				else{
					// Every other days 
					$this -> calculateBusinessTime("am");
					
					$this -> calculateBusinessTime("pm");
				}
			}
			else{
				// No action on holidays
				if($this->debug) echo "=> DAY OFF <=<br>";
			}

			if($this -> countdown == 0){
				// Exit from loop
				$complete = true;
			}
			else{
				// Current focus next day
				$this -> datetime = clone $this -> datetime -> modify('+1 day');
				// Next day
				$this -> businessTimeBegin["am"] = clone $this -> businessTimeBegin["am"] -> modify('+1 day');
				$this -> businessTimeEnd["am"] = clone $this -> businessTimeEnd["am"] -> modify('+1 day');
				$this -> businessTimeBegin["pm"] = clone $this -> businessTimeBegin["pm"] -> modify('+1 day');
				$this -> businessTimeEnd["pm"] = clone $this -> businessTimeEnd["pm"] -> modify('+1 day');
				// At the begining of the day
				$this -> datetime = clone $this->datetime->setTime(0, 0, 0);
			}
		}
    }
    
    function validateDate($date)
    {
    	$d = \DateTime::createFromFormat($this->format, $date);
    	return $d && $d->format($this->format) == $date;
    }
    
    private function calculateBusinessTime($period){
    	
    	if($this-> countdown == 0) return false; 
    	
    	$validTime = false;
    	
    	if($this -> businessTimeBegin[$period] -> getTimestamp() > $this -> datetime -> getTimestamp()){
    		// approve time is in business time interval?
    		$int = "OUT";
    		// Approved date is before the begin of the business time.
    		$diff = $this -> businessTimeBegin[$period] -> diff($this -> businessTimeEnd[$period]);
    		    		
    		$validTime = true;
    	}
    	elseif($this ->businessTimeEnd[$period] -> getTimestamp() > $this -> datetime -> getTimestamp()){
    		// approve time is in business time interval?
    		$int = "IN";
    		// Approved date is in the interval of business time
    		$diff = $this -> datetime ->diff($this -> businessTimeEnd[$period]);
    		
    		$validTime = true;
    	}
    	
    	// Valid business time 
    	if($validTime){
			
    		$min = ($diff -> h * 60) + $diff -> i;
    		
    		if($min > $this -> countdown){
    			$this -> datetime = clone $this -> businessTimeBegin[$period] -> add(new \DateInterval('PT' . $this -> countdown . 'M'));
    			$this -> countdown = 0;
    		}
    		else{
    			$this -> datetime  = clone $this -> businessTimeEnd[$period];
    			$this -> countdown -= $min;
    		}
    		
    		if($this->debug){
    			// Display the current progression
    			echo "[".$period."][".$int."] Time difference: " . $min . " | ".
    					"Countdown: " . $this -> countdown . "  / 240 | ".
    					"Datetime: " . $this -> datetime -> format($this -> format) . "<br>";
    		}
    	}
    }
}
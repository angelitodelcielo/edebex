<?php

namespace edebex;

class BusinessCalculator{

    private $datetime = null;
    private $debug = true;
    private $businessTime = array( 
        'am' => array('begin' => '09:00:00', 'end' => '12:00:00'),
        'pm' => array('begin' => '13:30:00', 'end' => '17:00:00')
    );
    private $format = "Y-m-d H:i:s";
    private $countdown = 60 * 4 ;

    public function __construct(){
        $this -> datetime = new \DateTime("now");
    }

    private function calculateTime2Send()
    {
    	include_once $this->get('kernel')->getRootDir() . '/../vendor/ics-parser/ICal.class.php';
    	
    	$isMonday = $this -> datetime ->format('l') == 'Monday';
    	$isThursday = $this -> datetime ->format('l') == 'Thursday';

    	$ical   = new \ICal($this->get('kernel')->getRootDir().'/Resources/ics_2015_belgium.ics');
		
    	$events = $ical->events();

		$isHoliday = false;
		foreach ($events as $event) {
			if($event['DTSTART'] > "20150000"){
			    'SUMMARY: ' . @$event['SUMMARY'] . "<br />\n";
			    'DTSTART: ' . $event['DTSTART'] . "<br>";
			    'DTEND: ' . $event['DTEND'] . "<br />\n";
			    'DTSTAMP: ' . $event['DTSTAMP'] . "<br />\n";
				
				if($event['DTSTART'] == $this -> datetime ->format("Ymd")){
					$isHoliday = true;
					break;
				}
			}
		}
		
		$complete = false;
		
		while(!$complete){
			
			if(!$isHoliday){
				
				if($isMonday){
					
					$this -> calculateBusinessTime("pm");
				}
				elseif($isThursday){
						
					$this -> calculateBusinessTime("am");
				}
				else{
					$this -> calculateBusinessTime("am");
					
					$this -> calculateBusinessTime("pm");
				}
			}

			if($this -> countdown == 0){
				$complete = true;
			}
			
			//$now->add(new DateInterval('P1D'));
		}
		return $this -> datetime;
    }
    
    private function calculateBusinessTime($period){
    	
    	if($this-> countdown == 0) return false;
    	 
    	echo "[ STARTING CALCULATE ]<br>";
    	// PM business Time
    	$businessTimeBegin = new \DateTime($this->businessTime[$period]['begin']);
    	$businessTimeEnd = new \DateTime($this->businessTime[$period]['end']);
    	// Approved date is before the begin of the business time.
    	if(strtotime($businessTimeBegin -> format($this -> format)) > strtotime($this -> datetime -> format($this -> format))){
    		//in that case, we calculate all difference time
    		$diff = $businessTimeBegin->diff($businessTimeEnd);
    
    		$min = ($diff->h * 60) + $diff->i;
    
    		if($min > $this -> countdown){
    			echo "!! Coundown lower then diff time <br>";
    			$this -> datetime  = $businessTimeBegin->add(new \DateInterval('PT' . $this -> countdown . 'M'));
    			$this -> countdown = 0;
    		}
    		else{
    			$this -> datetime  = $businessTimeEnd;
    			$this -> countdown -= $min;
    		}
    
    		if($this->debug){
    			echo "	[Before begin] Time difference: " . $min . "<br>";
    			echo "	New countdown: " . $this -> countdown . "<br>";
    			echo "	New datetime: " . $this -> datetime ->format($this -> format) . "<br>";
    		}
    	}
    	// Approved date is in the interval of business time
    	elseif(strtotime($businessTimeEnd -> format($this -> format)) > strtotime($this -> datetime -> format($this -> format))){
    		// Difference with the approval time.
    		$diff = $this -> datetime ->diff($businessTimeEnd);
    
    		$min = ($diff->h * 60) + $diff->i;
    
    		if($min > $this -> countdown){
    			echo "coundown lower then diff time <br>";
    			$this -> datetime = $businessTimeBegin->add(new \DateInterval('PT' . $this -> countdown . 'M'));
    			$this -> countdown = 0;
    		}
    		else{
    			$this -> datetime  = $businessTimeEnd;
    			$this -> countdown -= $min;
    		}
    
    		if($this->debug){
    			echo "	[In interval] Time difference: " . $min . "<br>";
    			echo "	New countdown: " . $this -> countdown . "<br>";
    			echo "	New datetime: " . $this -> datetime -> format($this -> format) . "<br>";
    		}
    	}
    	else{
    
    		if($this->debug){
    			echo "	[After End] No action <br>";
    		}
    	}
    	
    	echo "<hr>";    
    }
}